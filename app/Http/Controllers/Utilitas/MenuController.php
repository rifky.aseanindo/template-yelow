<?php

namespace App\Http\Controllers\Utilitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\SubMenu;
use App\Models\Permission;

class MenuController extends Controller
{

	public function index()
	{
		if(request()->ajax())
		{

		$data = Menu::whereNull('deleted_at')->orderBy('no_order', 'ASC')->get();

			return datatables()->of($data)
				->addColumn('is_parent', static function ($row) {
				if($row->is_parent == 1){
					return '<span class="badge bg-success">Yes</span>';
				}else{
					return '<span class="badge bg-danger">No</span>';
				}
				})
				->addColumn('idMenu', static function ($row) {
					return $row->uuid;
				})
				->rawColumns(['is_parent', 'idMenu'])
				->make(true);
		}

		return view('utilitas.menu.index');
	}

	public function create()
	{
		return view('utilitas.menu.create');
	}

	public function save(Request $request)
	{
		//validasi data
		$this->validate($request, [
			'name' => 'string|max:100|unique:menu',
			'icon' => 'nullable|string|max:100',
			'url' => 'nullable|string|max:100',
			'is_parent' => 'integer',
			'no_order' => 'integer|unique:menu'
		]);

		$menu = Menu::create([
			'name' => $request->name,
			'icon' => $request->icon,
			'url' => $request->url,
			'is_parent' => $request->is_parent,
			'no_order' => $request->no_order,
			'created_at' => date("Y-m-d H:i:s"),
			'created_by' => session('sess_user')->name
		]);

		if($request->is_parent == 2){
			SubMenu::create([
				'menu_id' => $menu->uuid,
				'status' => 0,
				'no_order' => 0,
				'created_at' => date("Y-m-d H:i:s"),
				'created_by' => session('sess_user')->name
			]);
		}

		return response()->json([
			'data' => $menu,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function delete(Request $request)
	{
		$menu = Menu::where('uuid', $request->id)->first();
		$submenu = SubMenu::where('menu_id', $menu->id)->delete();
		$permission = Permission::where('menu_name', str_replace(' ', '', strtolower($menu->name)))->delete();
		$menu->update([
			'deleted_by' => session('sess_user')->name
		]);
		$menu->delete();

		return response()->json([
         'data' => $menu,
         'success' => true,
         'alert' => 'danger',
         'message' => 'Successfully delete data'
      ]);
	}

   public function edit($id)
	{
	   $menu = Menu::where('uuid', $id)->first();
	   return view('utilitas.menu.edit', compact('menu'));
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'name' => 'string|max:100',
			'icon' => 'nullable|string|max:100',
			'url' => 'nullable|string|max:100',
			'is_parent' => 'integer',
			'no_order' => 'integer',
		]);

		$menu = Menu::findOrFail($request->id);
	
		$menu->update([
			'name' => $request->name,
			'icon' => $request->icon,
			'url' => $request->url,
			'is_parent' => $request->is_parent,
			'no_order' => $request->no_order,
			'updated_at' => date("Y-m-d H:i:s"),
			'updated_by' => session('sess_user')->name
		]);

		return response()->json([
			'data' => $menu,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
	}

	public function getDataMenu(Request $request)
	{
		$data = Menu::where('uuid', $request->id)->first();
		return response()->json($data);
	}
}
