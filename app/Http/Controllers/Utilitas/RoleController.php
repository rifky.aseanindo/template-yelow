<?php

namespace App\Http\Controllers\Utilitas;

use Illuminate\Http\Request;
// use Spatie\Permission\Models\Role;
use App\Models\Role;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
	public function index()
	{
		if(request()->ajax())
         {

         $data = Role::whereNull('deleted_at')->orderBy('name', 'ASC')->get();

            return datatables()->of($data)
                  ->addColumn('id', static function ($row) {
                     return $row->uuid;
                  })
                  ->rawColumns(['id'])
                  ->make(true);
         }

      return view('utilitas.role.index');
	}
	
	public function save(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|string|max:50'
		]);

		$role = Role::firstOrCreate([
			'name' => $request->name,
			'category' => 1,
		]);
		
		return response()->json([
         'data' => $role,
         'success' => true,
         'alert' => 'success',
         'message' => 'Successfully add data'
      ]);
	}

	public function delete(Request $request)
	{
		$role = Role::findOrFail($request->id);
		$role->update([
			'deleted_by' => session('sess_user')->name
		]);
		$role->delete();

		return response()->json([
         'data' => $role,
         'success' => true,
         'alert' => 'success',
         'message' => 'Successfully delete data'
      ]);
	}
}
