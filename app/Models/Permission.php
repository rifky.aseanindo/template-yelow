<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Uuid;

/**
 * Class User
 * 
 * @property string $uuid
 * @property string $name

 * @property Collection|Administrator[] $administrators
 * @property Collection|Customer[] $customers
 *
 * @package App\Models
 */
class Permission extends Authenticatable
{
   public $timestamps = false;
   public $incrementing = false;
	
	use Notifiable, HasRoles, Uuid;
	
	protected $table = 'permission';
	protected $primaryKey = 'uuid';
	protected $keyType = 'string';
	protected $casts = [
		'uuid' => 'string'
	];
	protected $fillable = [
      'uuid',
		'name',
		'menu_name'
	];

}
