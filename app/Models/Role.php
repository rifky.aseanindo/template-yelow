<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Uuid;

/**
 * Class User
 * 
 * @property string $uuid
 * @property string $name
 * @property Carbon|null $created_at
 * @property string|null $created_by
 * @property Carbon|null $updated_at
 * @property string|null $updated_by
 * @property Carbon|null $deleted_at
 * @property string|null $deleted_by
 * 
 * @property Collection|Administrator[] $administrators
 * @property Collection|Customer[] $customers
 *
 * @package App\Models
 */
class Role extends Authenticatable
{
	public $timestamps = false;
	public $incrementing = false;
	
	use SoftDeletes, Notifiable, HasRoles, Uuid;
	
	protected $table = 'role';
	protected $primaryKey = 'uuid';
	protected $keyType = 'string';
	protected $casts = [
		'uuid' => 'string'
	];
	protected $fillable = [
		'uuid',
		'name',
		'category',
	];

}
