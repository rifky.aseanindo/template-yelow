                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <script>
                                    document.write(new Date().getFullYear())
                                </script> © Yelow.
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-end d-none d-sm-block">
                                    Design & Develop by PT. Abinaya Madani Adyatama
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                </div>
                <!-- end main content-->

                </div>
                <!-- END layout-wrapper -->

                {{-- CKEditor --}}
                <script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
                <!-- JAVASCRIPT -->
                <script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/metismenu/metisMenu.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/simplebar/simplebar.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/node-waves/waves.min.js') }}"></script>

                <!-- apexcharts -->
                <script src="{{ asset('backend/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

                <!-- dashboard init -->
                <script src="{{ asset('backend/assets/js/pages/dashboard.init.js') }}"></script>

                <!-- Required datatable js -->
                <script src="{{ asset('backend/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
                <!-- Buttons examples -->
                <script src="{{ asset('backend/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/jszip/jszip.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/pdfmake/build/pdfmake.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/pdfmake/build/vfs_fonts.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>

                <!-- Responsive examples -->
                <script src="{{ asset('backend/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}">
                </script>

                <!-- Datatable init js -->
                <script src="{{ asset('backend/assets/js/pages/datatables.init.js') }}"></script>

                <!--tinymce js-->
                <script src="{{ asset('backend/assets/libs/tinymce/tinymce.min.js') }}"></script>

                <!-- init js -->
                <script src="{{ asset('backend/assets/js/pages/form-editor.init.js') }}"></script>

                <!-- form repeater js -->
                <script src="{{ asset('backend/assets/libs/jquery.repeater/jquery.repeater.min.js') }}"></script>
                <script src="{{ asset('backend/assets/js/pages/form-repeater.int.js') }}"></script>

                <!-- Sweet Alert -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
                
                <!-- Datepicker -->
                <script src="{{ asset('backend/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
                <script src="{{ asset('backend/assets/libs/@chenfengyuan/datepicker/datepicker.min.js') }}"></script>

                <!-- App js -->
                <script src="{{ asset('backend/assets/js/app-backend.js') }}"></script>
                <script type="text/javascript">
                    // $('input').attr('autocomplete', 'off');
                    setTimeout(function() {
                        $('#notification').fadeOut('slow');
                    }, 3000);

                    function konfirmasiHapus(ev) {
                        ev.preventDefault();
                        var urlHapus = ev.currentTarget.getAttribute('href');
                        swal({
                            title: "Are you sure want to delete this data?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                window.location.href = urlHapus;
                            } else {
                                swal("Hapus gagal.");
                            }
                        });
                    }

                    function konfirmasiLogout(ev) {
                        ev.preventDefault();
                        swal({
                            title: "Are you sure want to logout?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                document.getElementById('logout-form').submit();
                            } else {
                                swal("Logout failed.");
                            }
                        });
                    }

                </script>
                @yield('scripts')
                </body>

                </html>
