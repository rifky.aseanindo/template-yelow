@if (count($messages))
    <div class="row" style="padding-top: 10px;" id="notification">
        <div class="col-md-12">
            @foreach ($messages as $message)
                <div class="alert alert-{{ $message['level'] }}">{!! $message['message'] !!}</div>
            @endforeach
        </div>
    </div>
@endif