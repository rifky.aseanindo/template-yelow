@extends('layouts.master')

@section('title')
<title>Data User</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">List User</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Utility</a></li>
                                <li class="breadcrumb-item active">List User</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">
                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOne">
                                        <button class="accordion-button fw-medium collapsed btnForm" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                        Form
                                        </button>
                                    </h2>
                                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                        <input type="hidden" id="routeGetData" value="{{ route('user.get-data') }}">
                                        <input type="hidden" id="idEdit">
                                        <div class="accordion-body text-muted">
                                            <div class="row" id="fieldForm">
                                                <div class="col-lg-12">
                                                    <form id="formInput">
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Name</label>
                                                                    <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="name" required>
                                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Email</label>
                                                                    <input class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" type="email" name="email" id="email" required>
                                                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Password</label>
                                                                    <input class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" type="password" name="password" id="password" required>
                                                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                                                    <p class="text-success notifPassword" style="display: none;">Let this empty if you don't want to change password</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row fieldRole">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Role</label>
                                                                    <select class="form-select {{ $errors->has('role') ? 'is-invalid':'' }}" name="role" id="role" required>
                                                                        <option value="">Choose</option>
                                                                        @foreach ($role as $row)
                                                                        <option value="{{ $row->name }}">{{ $row->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <p class="text-danger">{{ $errors->first('role') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row fieldStatus" style="display: none;">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Status</label>
                                                                    <select class="form-select {{ $errors->has('status') ? 'is-invalid':'' }}" name="status" id="statusUser">
                                                                        <option>Active</option>
                                                                        <option value="1">Active</option>
                                                                        <option value="0">Suspend</option>
                                                                    </select>
                                                                    <p class="text-danger">{{ $errors->first('status') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-primary" id="btnSubmit">Submit</button>
                                                        <button type="button" class="btn btn-light" id="btnClear">Cancel</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="row" id="fieldUpdateRole" style="display: none;">
                                                <div class="col-lg-12">
                                                    <form id="formRole">
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Name</label>
                                                                    <input class="form-control" type="text" id="nameUpdateRole" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Email</label>
                                                                    <input class="form-control" type="email" id="emailUpdateRole" readonly>
                                                                    <p></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Role</label>
                                                                    <select class="form-select {{ $errors->has('roleUpdated') ? 'is-invalid':'' }}" name="roleUpdated" id="roleUpdated">
                                                                        <option value="">Choose</option>
                                                                        @foreach ($roles as $row)
                                                                        <option value="{{ $row->uuid }}">{{ $row->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <p class="text-danger">{{ $errors->first('roleUpdated') }}</p>
                                                                    {{-- @foreach ($roles as $row)
                                                                    <input type="radio" name="roleUpdate" id="roleUpdate" value="{{ $row->uuid }}"> {{  $row->name }} <br>
                                                                    @endforeach
                                                                    <p></p> --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-primary" id="btnSubmitUpdateRole">Submit</button>
                                                        <button type="button" class="btn btn-light" id="btnClearUpdateRole">Cancel</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingTwo">
                                        <button class="accordion-button fw-medium btnList" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="true" aria-controls="flush-collapseTwo">
                                        List Data
                                        </button>
                                    </h2>
                                    <div id="flush-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body text-muted">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="search-box me-2 mb-2 d-inline-block">
                                                        <div class="position-relative">
                                                            <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                                            <i class="bx bx-search-alt search-icon"></i>
                                                        </div>
                                                    </div>
                                                    <button id="btnSearch" type="button" style="background-color: #F6A500; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="d-flex flex-row-reverse mb-2">
                                                    <button type="button" class="btn btn-primary" id="btnAdd">Add New</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="padding-top: 10px;" id="notif">
                                            </div>
                                            <table id="table-user" class="table table-bordered dt-responsive nowrap w-100">
                                                <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Role</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                </thead>

                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/user/index.js') }}"></script>
@endsection
