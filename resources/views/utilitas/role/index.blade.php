@extends('layouts.master')

@section('title')
<title>Role Management</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">List Role</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Utility</a></li>
                                <li class="breadcrumb-item active">List Role</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="col-lg-4 p-1 pe-3">
                                    <div style="background-color: #F8F8FB;" class="card h-100">
                                        <div class="card-body ">
                                            <h5 class="mb-sm-3 font-size-18 ">Add Role</h5>
                                            <form id="formInput">
                                                <input type="hidden" id="routeGetData" value="{{ route('role.get-data') }}">
                                                <input type="hidden" id="routeSaveData" value="{{ route('role.save') }}">
                                                <div class="row">
                                                    <div class="col">
                                                        <div>
                                                            <label class="form-label">Name</label>
                                                            <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="roleName" required>
                                                            <p class="text-danger">{{ $errors->first('name') }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-primary" id="btnSave">Submit</button>
                                                <button type="button" class="btn btn-light" id="btnClear">Clear</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="col-lg-4 d-flex  align-items-center">
                                                <h5 class="card-title mb-0">List Role</h5>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <div class="search-box me-2  d-inline-block">
                                                    <div class="position-relative">
                                                        <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                                        <i class="bx bx-search-alt search-icon"></i>
                                                    </div>
                                                </div>
                                                <button id="btnSearch" type="button" style="background-color: #F6A500; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 10px;" id="notif">
                                    </div>
                                    <table id="table-role" class="table table-bordered dt-responsive nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
<!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/role/index.js') }}"></script>
@endsection