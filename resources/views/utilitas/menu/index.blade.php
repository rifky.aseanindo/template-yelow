@extends('layouts.master')

@section('title')
<title>Data Menu</title>
@endsection

@section('content')
<!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18">List Menu</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Utility</a></li>
                                    <li class="breadcrumb-item active">List Menu</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                <div class="accordion accordion-flush" id="accordionFlushExample">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingOne">
                                            <button class="accordion-button fw-medium collapsed btnForm" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                            Form
                                            </button>
                                        </h2>
                                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                            <div class="accordion-body text-muted">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <form id="formInput">
                                                        <input type="hidden" id="routeGetData" value="{{ route('menu.get-data') }}">
                                                        <input type="hidden" id="routeSaveData" value="{{ route('menu.save') }}">
                                                        <input type="hidden" id="idEdit">
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Name</label>
                                                                    <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="name" required>
                                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Icon</label>
                                                                    <input class="form-control {{ $errors->has('icon') ? 'is-invalid':'' }}" type="text" name="icon" id="icon">
                                                                    <p class="text-danger">{{ $errors->first('icon') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Url</label>
                                                                    <input class="form-control {{ $errors->has('url') ? 'is-invalid':'' }}" type="text" name="url" id="url" required>
                                                                    <p class="text-danger">{{ $errors->first('url') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Parent</label>
                                                                    <select class="form-select {{ $errors->has('is_parent') ? 'is-invalid':'' }}" name="is_parent" id="is_parent" required>
                                                                        <option value="">Pilih</option>
                                                                        <option value="1">Yes</option>
                                                                        <option value="2">No</option>
                                                                    </select>
                                                                    <p class="text-danger">{{ $errors->first('is_parent') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Order Number</label>
                                                                    <input class="form-control {{ $errors->has('no_order') ? 'is-invalid':'' }}" type="number" name="no_order" id="no_order">
                                                                    <p class="text-danger">{{ $errors->first('no_order') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-primary" id="btnSubmit">Submit</button>
                                                        <button type="button" class="btn btn-light" id="btnClear">Cancel</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingTwo">
                                            <button class="accordion-button fw-medium btnList" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="true" aria-controls="flush-collapseTwo">
                                            List Data
                                            </button>
                                        </h2>
                                        <div id="flush-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                            <div class="accordion-body text-muted">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="search-box me-2 mb-2 d-inline-block">
                                                            <div class="position-relative">
                                                                <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                                                <i class="bx bx-search-alt search-icon"></i>
                                                            </div>
                                                        </div>
                                                        <button id="btnSearch" type="button" style="background-color: #F6A500; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="d-flex flex-row-reverse mb-2">
                                                        <button type="button" class="btn btn-primary" id="btnAdd">Add New</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="padding-top: 10px;" id="notif">
                                                </div>
                                                <table id="table-menu" class="table table-bordered dt-responsive nowrap w-100">
                                                    <thead>
                                                        <tr>
                                                        <th>Name</th>
                                                        <th>Icon</th>
                                                        <th>Url</th>
                                                        <th>Parent</th>
                                                        <th>Menu Order</th>
                                                        <th>Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
    <!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/menu/index.js') }}"></script>
@endsection
