<?php

use Illuminate\Support\Facades\Route;

Route::get('/permission-error', 'HomeController@permissionError')->name('permission-error');

Route::group(['middleware'=>['cekRole:superadmin|finance']], function () {   
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['prefix' => 'utilitas', 'middleware'=>['cekRole:superadmin|admin|owner']], function () {
    // User
    Route::get('/user', 'Utilitas\UserController@index')->name('user.index');
    Route::post('/user/save', 'Utilitas\UserController@save')->name('user.save');
    Route::post('/user/update', 'Utilitas\UserController@update')->name('user.update');
    Route::get('/user/edit/{id}', 'Utilitas\UserController@edit')->name('user.edit');
    Route::get('/user/delete', 'Utilitas\UserController@delete')->name('user.delete');
    Route::post('/user/update-role', 'Utilitas\UserController@updateRole')->name('user.update-role');
    Route::post('/user/get-data', 'Utilitas\UserController@getData')->name('user.get-data');
    
    Route::get('/user/roles/{id}', 'Utilitas\UserController@roles')->name('user.roles');
    Route::put('/user/roles/{id}', 'Utilitas\UserController@setRole')->name('user.set_role');
    Route::post('/user/permission', 'Utilitas\UserController@addPermission')->name('user.add_permission');
    Route::get('/user/role-permission', 'Utilitas\UserController@rolePermission')->name('user.roles_permission');
    Route::put('/user/permission/{role}', 'Utilitas\UserController@setRolePermission')->name('user.setRolePermission');
    Route::post('/user/cek-email', 'Utilitas\UserController@cekEmail')->name('user.cek-email');
    Route::post('/user/cek-password', 'Utilitas\UserController@cekPassword')->name('user.cek-password');
    
    // Role
    Route::get('/role', 'Utilitas\RoleController@index')->name('role.index');
    Route::post('/role/save', 'Utilitas\RoleController@save')->name('role.save');
    Route::get('/role/delete', 'Utilitas\RoleController@delete')->name('role.delete');
    Route::post('/role/get-data', 'Utilitas\RoleController@getData')->name('role.get-data');

    //Menu
    Route::get('/menu', 'Utilitas\MenuController@index')->name('menu.index');
    Route::post('/menu/save', 'Utilitas\MenuController@save')->name('menu.save');
    Route::post('/menu/update', 'Utilitas\MenuController@update')->name('menu.update');
    Route::get('/menu/delete', 'Utilitas\MenuController@delete')->name('menu.delete');
    Route::post('/menu/get-data', 'Utilitas\MenuController@getDataMenu')->name('menu.get-data');
    
    //SubMenu
	Route::get('/submenu', 'Utilitas\SubMenuController@index')->name('submenu.index');
	Route::post('/submenu/save', 'Utilitas\SubMenuController@save')->name('submenu.save');
	Route::post('/submenu/update', 'Utilitas\SubMenuController@update')->name('submenu.update');
	Route::get('/submenu/delete', 'Utilitas\SubMenuController@delete')->name('submenu.delete');
    Route::post('/submenu/get-data', 'Utilitas\SubMenuController@getData')->name('submenu.get-data');
});