<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\RoleHasPermission;
use App\Models\Role;

class RoleHasPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
   {
      $role = Role::where('name', 'superadmin')->first();
      $permission = Permission::get();

      foreach($permission as $data){
         RoleHasPermission::create([
            'permission_id' => $data->uuid,
            'role_id' => $role->uuid
         ]);
      };
      
   }
}
