<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\SubMenu;

class SubMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
   {
      $menuNoParent = Menu::where('is_parent', 2)->get();
      $menuBa = Menu::where('name', 'like', '%business account%')->first();
      $menuPackage = Menu::where('name', 'like', '%package%')->first();
      $menuBilling = Menu::where('name', 'like', '%billing%')->first();
      $menuReport = Menu::where('name', 'like', '%report%')->first();
      $menuMaster = Menu::where('name', 'like', '%master%')->first();

      $stringBa = ['Verification', 'Approved'];
      $stringPackage = ['Help Desk', 'In-App Call', 'Additional In-App Call', 'Additional Online Account', 'Report', 'Information Banner'];
      $stringBilling = ['Package', 'Customer'];
      $stringReport = ['Package', 'Customer'];
      $stringMaster = ['Category', 'Bank', 'Transaction Fee', 'Banner', 'FAQ', 'Terms & Condition', 'Privacy Policy'];

      foreach($menuNoParent as $noParent){
         SubMenu::create([
            'menu_id' => $noParent->uuid,
            'no_order' => 0,
            'status' => 0,
            'created_at' => now(),
            'created_by' => 'system',
         ]);
      }

      foreach($stringBa as $key => $value){
         $order = $key + 1;
         SubMenu::create([
            'menu_id' => $menuBa->uuid,
            'name' => $value,
            'url' => '#',
            'no_order' => $order,
            'status' => 1,
            'created_at' => now(),
            'created_by' => 'system',
         ]);
      }

      foreach($stringPackage as $key => $value){
         $order = $key + 1;
         SubMenu::create([
            'menu_id' => $menuPackage->uuid,
            'name' => $value,
            'url' => '#',
            'no_order' => $order,
            'status' => 1,
            'created_at' => now(),
            'created_by' => 'system',
         ]);
      }

      foreach($stringBilling as $key => $value){
         $order = $key + 1;
         SubMenu::create([
            'menu_id' => $menuBilling->uuid,
            'name' => $value,
            'url' => '#',
            'no_order' => $order,
            'status' => 1,
            'created_at' => now(),
            'created_by' => 'system',
         ]);
      }

      foreach($stringReport as $key => $value){
         $order = $key + 1;
         SubMenu::create([
            'menu_id' => $menuReport->uuid,
            'name' => $value,
            'url' => '#',
            'no_order' => $order,
            'status' => 1,
            'created_at' => now(),
            'created_by' => 'system',
         ]);
      }

      foreach($stringMaster as $key => $value){
         $order = $key + 1;
         SubMenu::create([
            'menu_id' => $menuMaster->uuid,
            'name' => $value,
            'url' => '#',
            'no_order' => $order,
            'status' => 1,
            'created_at' => now(),
            'created_by' => 'system',
         ]);
      }

   }
}
