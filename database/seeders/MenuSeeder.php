<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\SubMenu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::create([
            'name' => 'Business Account',
            'icon' => 'bx bx-building-house',
            'url' => '#',
            'no_order' => 1,
            'is_parent' => 1,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        Menu::create([
            'name' => 'User Account',
            'icon' => 'bx bx-user',
            'url' => '/user-account',
            'no_order' => 2,
            'is_parent' => 2,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        Menu::create([
            'name' => 'Package',
            'icon' => 'bx bx-package',
            'url' => '#',
            'no_order' => 3,
            'is_parent' => 1,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        Menu::create([
            'name' => 'Billing',
            'icon' => 'bx bx-spreadsheet',
            'url' => '#',
            'no_order' => 4,
            'is_parent' => 1,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        Menu::create([
            'name' => 'Report',
            'icon' => 'bx bx-sitemap',
            'url' => '#',
            'no_order' => 5,
            'is_parent' => 1,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        Menu::create([
            'name' => 'Recording',
            'icon' => 'bx bx-volume-full',
            'url' => '/recording',
            'no_order' => 6,
            'is_parent' => 2,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        Menu::create([
            'name' => 'Account Management',
            'icon' => 'bx bx-group',
            'url' => '/account-management',
            'no_order' => 7,
            'is_parent' => 2,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        Menu::create([
            'name' => 'Marketing',
            'icon' => 'bx bx-trending-up',
            'url' => '/marketing',
            'no_order' => 8,
            'is_parent' => 2,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

        
        Menu::create([
            'name' => 'Master Data',
            'icon' => 'bx bx-server',
            'url' => '#',
            'no_order' => 9,
            'is_parent' => 1,
            'created_at' => now(),
            'created_by' => 'system',
        ]);

    }
}
