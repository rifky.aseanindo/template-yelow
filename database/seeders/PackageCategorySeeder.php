<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PackageCategory;

class PackageCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $stringPackage = ['Help Desk', 'In-App Call', 'Information Banner', 'Additional In-App Call', 'Additional Online Account', 'Report'];

      foreach($stringPackage as $key => $data){
         $order = $key + 1;
         PackageCategory::create([
            'name' => $data,
            'no_order' => $order,
            'created_at' => now(),
            'created_by' => 'system',
         ]);
      }

    }
}
