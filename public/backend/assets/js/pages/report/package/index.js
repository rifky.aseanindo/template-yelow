$(function () {
   showTable();

   function showTable() {
      var dataTable = $("#table-report").DataTable({
         language: {
               paginate: {
                  next: '<i class="fas fa-chevron-right"></i>',
                  previous: '<i class="fas fa-chevron-left"></i>'
               }
         },
         processing: false,
         bLengthChange: false,
         bInfo: false,
         pageLength: 10,
         ajax: {
               url: "/report/package",
         },
         columns: [
               { data: "created_date" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               { data: "invoice_number" },
               // {
               //    data: "action",
               //    orderable: false,
               //    searchable: false,
               //    render: function (data, type, row) {
               //       var id = row.id;
               //       return (
               //             `
               //                <a href="/report/package/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
               //                <a href="/report/package/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
               //             `
               //       );
               //    },
               // },
         ],
         searchDelay: 750,
         buttons: [],
         columnDefs: [
               {
                  defaultContent: "-",
                  targets: "_all",
                  className: "text-left",
               },
         ],
         bDestroy: true,
      });
      $(".dataTables_filter").hide();

      $("#btnSearch").click(function name() {
         let valInput = document.getElementById("myInputTextField").value;
         dataTable.search(valInput).draw();
      });
   }

});
