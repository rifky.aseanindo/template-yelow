const token = $('meta[name="csrf-token"]').attr('content')
const routeSaveData = $("#routeSaveData").val()

$(function () {
    $('#btnSubmit').prop("disabled", true)    
    $('#button_add_details').prop("disabled", true)

    $('#price').on('change', function(e){
        if($(this).val() != ''){
            $('#button_add_details').prop("disabled", false)
        }
    })

    $('#input_details_0').on('change', function(e){
        if($('#name').val() != '' && $('#price').val() != '' && $(this).val() != ''){
            $('#btnSubmit').prop("disabled", false)
        }
    })

    // Repeater field description
    $('#button_add_details').on('click', function(e) {
        e.preventDefault()
        let indexDetail = 0
        $('#list_detail').children().each(function() {
            let index = $(this).attr('data-index-detail')
            if (index > indexDetail) {
                indexDetail = index
            }
        });
        indexDetail = Number(indexDetail) + 1
        const newDetail = `
            <div class="inner-repeater mb-3" data-index-detail="${indexDetail}">
                <div data-repeater-list="inner-group" class="inner mb-3">
                    <div data-repeater-item class="inner mb-3 row">
                        <div class="col-md-8 col-8 d-flex h-100 align-items-center">
                            <div style="border-radius: 100px; height: 25px; width: 25px; text-align: center; display: flex; justify-content: center; align-items: center; background-color: #F4AE00; color: #fff;" class="me-2">
                                <i class="fas fa-check"></i>
                            </div>
                            <input type="text" name="details[${indexDetail}]" id="input_details_${indexDetail}" class="inner form-control" placeholder="Enter here..."/>
                        </div>
                        <div class="col-md-1 col-4">
                            <div class="d-grid">
                                <button type="button" data-type="btn-remove" data-index-detail="${indexDetail}" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `;
            $('#list_detail').append(newDetail)
            $('#input_details_'+indexDetail).focus()
            $('#input_details_'+indexDetail).on('change', function(e){
                if($(this).val() != ''){
                    $('#btnSubmit').prop("disabled", false)
                }
            })
        });

    //  Event button remove
    $(document).on('click', "button[data-type='btn-remove']", function(e) {
        e.preventDefault();
        let indexDetail = $(this).attr('data-index-detail');
        if ($('#list_detail').children().length !== 1) {
            $(`div[data-index-detail="${indexDetail}"]`).remove();
        }
    });

    $('#btnSubmit').on('click', function (e) {
        e.preventDefault()
        var url = routeSaveData
        var loopDetail = []
        var indexDetail = 0
        $('#list_detail').children().each(function () {
            var index = $(this).attr('data-index-detail')
            if (index > indexDetail) {
                indexDetail = index
            }
            loopDetail.push($('#input_details_'+indexDetail).val())
        })
        var data = {
                    name:$("#name").val(),
                    price:$("#price").val(),
                    details:loopDetail,
            } 

        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: url,
            data: data,
            success: function (response) {
                console.log(response.message)
                window.location = `/package/additional-in-app-call`;
            },
            error: function(xhr) {
                var err = JSON.parse(xhr.responseText)
                var errorString = '<ul>'
                $.each( err.errors, function( key, value) {
                    errorString += '<p>' + value + '</p>'
                })
                errorString += '</ul>'
                
                swal({
                    type: "warning",
                    title: errorString,
                    showCancelButton: false,
                    confirmButtonColor: "#e3342f",
                    confirmButtonText: "Ok",
                })
            }
        })    
    });
});
