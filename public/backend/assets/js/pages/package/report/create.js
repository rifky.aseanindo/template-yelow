const token = $('meta[name="csrf-token"]').attr('content')
const routeSaveData = $("#routeSaveData").val()

$(function () {
   if($('#idEdit').val() == null){
      $('#btnSubmit').prop("disabled", true)    
      $('#button_add_details').prop("disabled", true)
      $('#price').on('change', function(e){
         if($(this).val() != ''){
            $('#btnSubmit').prop("disabled", false)    
            $('#btnSubmit').focus()    
         }
      })
   }

   $('#btnSubmit').on('click', function (e) {
      e.preventDefault()
      var url = routeSaveData
      if($('#idEdit').val() != null){
         var type = 'PUT' 
         var data = {
                     id:$('#idEdit').val(),
                     name:$("#name").val(),
                     price:$("#price").val(),
                  }
      }else{
         var type = 'POST' 
         var data = {
                     name:$("#name").val(),
                     price:$("#price").val(),
                  }
      }

      $.ajax({
         headers: { 'X-CSRF-TOKEN': token },
         type: type,
         url: url,
         data: data,
         success: function (response) {
               console.log(response.message)
               window.location = `/package/report`;
         },
         error: function(xhr) {
               var err = JSON.parse(xhr.responseText)
               var errorString = '<ul>'
               $.each( err.errors, function( key, value) {
                  errorString += '<p>' + value + '</p>'
               })
               errorString += '</ul>'
               
               swal({
                  type: "warning",
                  title: errorString,
                  showCancelButton: false,
                  confirmButtonColor: "#e3342f",
                  confirmButtonText: "Ok",
               })
         }
      })    
   });
});
