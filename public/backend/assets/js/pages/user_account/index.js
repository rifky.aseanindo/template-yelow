$(function () {
    tableActive();

    function tableActive() {
        var dataTableActive = $("#table-active").DataTable({
            language: {
                paginate: {
                    next: '<i class="fas fa-chevron-right"></i>',
                    previous: '<i class="fas fa-chevron-left"></i>'
                }
            },
            processing: false,
            bLengthChange: false,
            bInfo: false,
            pageLength: 10,
            ajax: {
                url: "/user-account/active",
            },
            columns: [
                { data: "created_date" },
                { data: "name" },
                { data: "email" },
                {
                    data: "action",
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        var id = row.id;
                        return (
                            `
                                <a href="/user-account/detail" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                                <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                                <a href="/user-account/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                            `
                        );
                    },
                },
            ],
            searchDelay: 750,
            buttons: [],
            columnDefs: [
                {
                    defaultContent: "-",
                    targets: "_all",
                    className: "text-left",
                },
            ],
            bDestroy: true,
        });

        $(".dataTables_filter").hide();

        $("#btnSearch").click(function name() {
            let valInput = document.getElementById("myInputTextField").value;
            dataTableActive.search(valInput).draw();
        });
    }

    function tableSuspend() {
        var dataTableSuspend = $("#table-suspend").DataTable({
            language: {
                paginate: {
                    next: '<i class="fas fa-chevron-right"></i>',
                    previous: '<i class="fas fa-chevron-left"></i>'
                }
            },
            processing: false,
            bLengthChange: false,
            bInfo: false,
            pageLength: 10,
            ajax: {
                url: "/user-account/suspended",
            },
            columns: [
                { data: "created_date" },
                { data: "name" },
                { data: "email" },
                {
                    data: "action",
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        var id = row.id;
                        return (
                            `
                                <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                                <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                                <a href="/user-account/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                            `
                        );
                    },
                },
            ],
            searchDelay: 750,
            buttons: [],
            columnDefs: [
                {
                    defaultContent: "-",
                    targets: "_all",
                    className: "text-left",
                },
            ],
            bDestroy: true,
        });

        $(".dataTables_filter").hide();

        $("#btnSearch").click(function name() {
            let valInput = document.getElementById("myInputTextField").value;
            dataTableSuspend.search(valInput).draw();
        });
    }

    $("#showActive").on("click", function (e) {
        $("#tab-active-user").show();
        $("#tab-suspend-user").hide();
        tableActive();
    });
    $("#showSuspend").on("click", function (e) {
        $("#tab-active-user").hide();
        $("#tab-suspend-user").show();
        tableSuspend();
    });
});
