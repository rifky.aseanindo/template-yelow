const token = $('meta[name="csrf-token"]').attr('content')
const routeGetData = $("#routeGetData").val()
const routeSave = $("#routeSave").val()

displayData()

function displayData(){
   $("#table-account-access").DataTable({
      processing: false,
      ordering: false,
      paging: false,
      bLengthChange: false,
      bInfo: false,
      stripeClasses: ['stripe-color', 'stripe-2'],
      buttons: [],
      columnDefs: [
         {
               defaultContent: "-",
               targets: "_all",
               className: "text-left",
         },
      ],
      bDestroy: true,
   });

   $(".dataTables_filter").hide();
}

$('#btnSet').on('click', function (e) {
   e.preventDefault()
   var chekedValue = [];
   $('.role-permission:checked').each(function(){
      chekedValue .push($(this).val());
   })
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeSave,
      data: { data : chekedValue},
      success: function (response) {
         swal({
               type: "success",
               title: 'Set permission is successfully',
               showCancelButton: false,
               confirmButtonText: "Ok",
         })
         displayData()
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});