const token = $('meta[name="csrf-token"]').attr('content')
const routeGetData = $("#routeGetData").val()
let accordionOne = document.getElementById("flush-collapseOne")
let accordionTwo = document.getElementById("flush-collapseTwo")

displayData()

function displayData(){
   var dataTable = $("#table-user").DataTable({
      language: {
         paginate: {
               next: '<i class="fas fa-chevron-right"></i>',
               previous: '<i class="fas fa-chevron-left"></i>'
         }
      },
      processing: false,
      bLengthChange: false,
      bInfo: false,
      pageLength: 10,
      stripeClasses: ['stripe-color', 'stripe-2'],
      ajax: {
         url: "/utilitas/user",
      },
      columns: [
         { data: "name" },
         { data: "email" },
         { data: "role" },
         { data: "status" },
         {
               data: "action",
               orderable: false,
               searchable: false,
               render: function (data, type, row) {
                  var id = row.idUser;
                  return (
                     `
                        <div class="d-flex">
                           <a class="btn-circle text-success me-1" href="javascript:void(0)" id="`+id+`" onclick="role(event)"><i class="fas fa-user-cog"></i></a>
                           <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="edit(event)"><i class="fas fa-pen"></i></a>
                           <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                        </div>
                     `
                  );
               },
         },
      ],
      searchDelay: 750,
      buttons: [],
      columnDefs: [
         {
               defaultContent: "-",
               targets: "_all",
               className: "text-left",
         },
      ],
      bDestroy: true,
      // order: [[5, "ASC"]],
   });

   $(".dataTables_filter").hide();
   
   $("#btnSearch").click(function name() {
      let valInput = document.getElementById("myInputTextField").value;
      dataTable.search(valInput).draw();
   });
}

function edit(ev) {
   document.title = 'Edit User'
   $('.fieldStatus').show()
   accordionOne.classList.add("show")
   accordionTwo.classList.remove("show")
   
   ev.preventDefault();
   let idUser = ev.currentTarget.getAttribute('id');
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeGetData,
      data: {id: idUser},
      success: function (response) {
         $('.notifPassword').show()
         $('#idEdit').val(idUser)
         $('#name').val(response.name)
         $('#email').val(response.email)
         $('#password').val('')
         $('#statusUser').val(response.status)
         $('#role').val(response.roleName)
         $('.fieldStatus').hide()
      }
   })
}

function role(ev) {
   document.title = 'Update Role'
   $('#fieldForm').hide()
   $('#fieldUpdateRole').show()
   accordionOne.classList.add("show")
   accordionTwo.classList.remove("show")

   ev.preventDefault();
   let idUser = ev.currentTarget.getAttribute('id');
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeGetData,
      data: {id: idUser},
      success: function (response) {
         $('#idEdit').val(idUser)
         $('#nameUpdateRole').val(response.name)
         $('#emailUpdateRole').val(response.email)
         $('#roleUpdated').val(response.idRole)
      }
   })
}

$('#btnSubmit').on('click', function (e) {
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
   e.preventDefault()
   let idUser = $('#idEdit').val()

   if(idUser.length >= 7){
      var url = "/utilitas/user/update"
      var data = {
               name:$("#name").val(),
               email:$("#email").val(),
               password:$("#password").val(),
               role:$("#role").val(),
               status:$("#statusUser").val(),
               id:idUser
         } 
   }else{
      var url = "/utilitas/user/save"
      var data = {
               name:$("#name").val(),
               email:$("#email").val(),
               password:$("#password").val(),
               role:$("#role").val()          
         }
   }

   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: url,
      data: data,
      success: function (response) {
         document.title = 'Data User'
         document.getElementById("formInput").reset()
         const notifAlert = `
                              <div class="col-md-12 div`+response.data.uuid+`">
                                 <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                              </div>
                           `
         $('#notif').append(notifAlert)
         setTimeout(function() {
               $('.div'+response.data.uuid).fadeOut('slow')
         }, 1500)
         displayData()
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});

$('#btnSubmitUpdateRole').on('click', function (e) {
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
   e.preventDefault()
   let idUser = $('#idEdit').val()
   var url = "/utilitas/user/update-role"
   var data = {
         roleUpdated:$("#roleUpdated").val(),
         id:idUser
      }

   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: url,
      data: data,
      success: function (response) {
         document.title = 'Data User'
         document.getElementById("formRole").reset()
         const notifAlert = `
                              <div class="col-md-12 div`+response.data.uuid+`">
                                 <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                              </div>
                           `
         $('#notif').append(notifAlert)
         setTimeout(function() {
               $('.div'+response.data.uuid).fadeOut('slow')
         }, 1500)
         displayData()
      },
      error: function(xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each( err.errors, function( key, value) {
               errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'
         
         swal({
               type: "warning",
               title: errorString,
               showCancelButton: false,
               confirmButtonColor: "#e3342f",
               confirmButtonText: "Ok",
         })
      }
   })    
});

$('#btnClear').on('click', function (e) {
   document.title = 'Data User'
   $('.notifPassword').hide()
   $('#idEdit').val('')
   document.getElementById("formInput").reset()
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
});

$('#btnClearUpdateRole').on('click', function (e) {
   document.title = 'Data User'
   $('#fieldForm').show()
   $('#fieldUpdateRole').hide()
   $('#idEdit').val('')
   document.getElementById("formRole").reset()
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
});

$('#btnAdd').on('click', function (e) {
   document.title = 'Add User'
   accordionOne.classList.add("show")
   accordionTwo.classList.remove("show")
});

$('.btnForm').on('click', function(e){
   document.title = 'Add User'
})

$('.btnList').on('click', function(e){
   document.title = 'Data User'
})

function hapus(ev) {
   ev.preventDefault();
   let idUser = ev.currentTarget.getAttribute('id');
   swal({
      title: "Are you sure want to delete this data?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
   })
   .then((willDelete) => {
      if (willDelete) {
         $.ajax({
               headers: { 'X-CSRF-TOKEN': token },
               type: 'GET',
               url: "/utilitas/user/delete/",
               data: {id: idUser},
               success: function (response) {
                  const notifAlert = `
                           <div class="col-md-12 div`+response.data.uuid+`">
                              <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                           </div>
                     `
                  $('#notif').append(notifAlert)
                  setTimeout(function() {
                     $('.div'+response.data.uuid).fadeOut('slow')
                  }, 1500)
                  displayData()
               }
         })
      } else {
         swal("Hapus gagal.");
      }
   });
}