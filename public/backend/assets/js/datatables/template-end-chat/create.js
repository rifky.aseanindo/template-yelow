const token = $('meta[name="csrf-token"]').attr('content')
const routeSaveData = $("#routeSaveData").val()

$(function () {
    $('#fieldDescription').on('keyup', function (e) {
        if ($('#fieldDescription').val() == '') {
            $('.btnSubmit').prop("disabled", true)
        } else {
            $('.btnSubmit').prop("disabled", false)
        }
    });

    $('.btnSubmit').on('click', function (e) {
        e.preventDefault()
        var url = routeSaveData
        var data = {
            description: $('#fieldDescription').val()
        }
        
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                let hasil = data
                if (hasil == true) {
                    $('#modalForm').modal('hide')
                    displayData()
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    });

    //  Event button remove
    $(document).on('click', '.btnRemove', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');
        let url = $("#routeDeleteData").val()
        let data = {
            id: id
        }

        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                let hasil = data
                if (hasil == true) {
                    displayData()
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    });

    $('btnSubmitFinal').on('click', function (e) {
        e.preventDefault()
        var url = routeSaveData
        var data = {
            description: $('#fieldDescription').val()
        }

        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                let hasil = data
                if (hasil == true) {
                    $('#modalForm').modal('hide')
                    displayData()
                }
            },
            error: function (error) {
                console.log(error)
            }
        })
    });
});
