const token = $('meta[name="csrf-token"]').attr('content')
const routeGetDataFee = $("#routeGetDataFee").val()
const routeSaveData = $("#routeSaveData").val()

displayData()

function displayData(){
    var dataTable = $("#table-fee").DataTable({
        language: {
            paginate: {
                next: '<i class="fas fa-chevron-right"></i>',
                previous: '<i class="fas fa-chevron-left"></i>'
            }
        },
        processing: false,
        bLengthChange: false,
        bInfo: false,
        pageLength: 10,
        stripeClasses: ['stripe-color', 'stripe-2'],
        ajax: {
            url: "/master/fee",
        },
        columns: [
            { data: "name" },
            { data: "value" },
            {
                data: "action",
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    var id = row.idFee;
                    return (
                        `
                    <div class="d-flex">
                        <a class="btn-circle text-primary me-1" href="javascript:void(0)" id="`+id+`" onclick="edit(event)"><i class="fas fa-pen"></i></a>
                        <a class="btn-circle text-danger" href="javascript:void(0)" id="`+id+`" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                    </div>
                            
                        `
                    );
                },
            },
        ],
        searchDelay: 750,
        buttons: [],
        columnDefs: [
            {
                defaultContent: "-",
                targets: "_all",
                className: "text-left",
            },
        ],
        bDestroy: true,
    });

    $(".dataTables_filter").hide();
    
    $("#btnSearch").click(function name() {
        let valInput = document.getElementById("myInputTextField").value;
        dataTable.search(valInput).draw();
    });
}

function edit(ev) {
    document.title = 'Edit Transaction Fee'
    ev.preventDefault()
    let idFee = ev.currentTarget.getAttribute('id')
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: routeGetDataFee,
        data: {id: idFee},
        success: function (response) {
            const routeEdit = "/master/fee/edit/"+idFee 
            $('#routeSaveData').val(routeEdit)
            $('#namaFee').val(response.name)
            $('#nilaiFee').val(response.price)
            response.is_percent == 1 ? document.getElementById("switch5").checked = true : document.getElementById("switch5").checked = false
        }
    })
}

$('#nilaiFee').on('change', function(e){
    if($(".isPercent").is(':checked') && $(this).val() > 100){
        $('#nilaiFee').focus()
        swal({
            type: "warning",
            title: "Maximum percentage is 100%",
            showCancelButton: false,
            confirmButtonColor: "#e3342f",
            confirmButtonText: "Ok",
        })
        $('#nilaiFee').val(0)
    }
})

$('#btnSave').on('click', function (e) {
    e.preventDefault()
    const routeSaveData = $("#routeSaveData").val()
    let idFee = routeSaveData.substring(routeSaveData.lastIndexOf('/') + 1)
    $(".isPercent").is(':checked') ? $(".isPercent").val(1) : $(".isPercent").val(2)
    if($(".isPercent").is(':checked') && $('#nilaiFee').val() > 100){
        $('#nilaiFee').focus()
        swal({
            type: "warning",
            title: "Maximum percentage is 100%",
            showCancelButton: false,
            confirmButtonColor: "#e3342f",
            confirmButtonText: "Ok",
        })
        $('#nilaiFee').val(0)
    }
        
    if(idFee.length >= 7){
        var url = "/master/fee/update"
        var data = {
                    id:idFee,
                    name:$("#namaFee").val(),
                    price:$("#nilaiFee").val(),
                    is_percent:$(".isPercent").val()
                }
    }else{
        var url = "/master/fee/save"
        var data = {
            name:$("#namaFee").val(),
            price:$("#nilaiFee").val(),
            is_percent:$(".isPercent").val()          
        }
    }

    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: url,
        data: data,
        success: function (response) {
            document.title = 'Data Transaction Fee'
            document.getElementById("formInput").reset()
            const notifAlert = `
                                <div class="col-md-12 div`+response.data.uuid+`">
                                    <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                                </div>
                            `
            $('#notif').append(notifAlert)
            setTimeout(function() {
                $('.div'+response.data.uuid).fadeOut('slow')
            }, 1500)
            displayData()
            const route = "/master/fee/save" 
            $('#routeSaveData').val(route)
        },
        error: function (xhr) {
            var err = JSON.parse(xhr.responseText)
            var errorString = '<ul>'
            $.each( err.errors, function( key, value) {
                errorString += '<p>' + value + '</p>'
            })
            errorString += '</ul>'
            
            swal({
                type: "warning",
                title: errorString,
                showCancelButton: false,
                confirmButtonColor: "#e3342f",
                confirmButtonText: "Ok",
            })
        }
    })    
});

$('#btnClear').on('click', function (e) {    
    document.title = 'Data Transaction Fee'
    $('#namaFee').val('')
    document.getElementById("switch5").checked = false
    $('#nilaiFee').val('')
    const route = "/master/fee/save" 
    $('#routeSaveData').val(route)
});

function hapus(ev) {
    ev.preventDefault();
    let idFee = ev.currentTarget.getAttribute('id');
    swal({
        title: "Are you sure want to delete this data?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                headers: { 'X-CSRF-TOKEN': token },
                type: 'GET',
                url: "/master/fee/delete/",
                data: {id: idFee},
                success: function (response) {
                    const notifAlert = `
                        <div class="col-md-12 div`+response.data.uuid+`">
                            <div class="alert alert-`+response.alert+`">`+response.message+`</div>
                        </div>
                    `
                    $('#notif').append(notifAlert)
                    setTimeout(function() {
                        $('.div'+response.data.uuid).fadeOut('slow')
                    }, 1500)
                    displayData()
                }
            })
        } else {
            swal("Hapus gagal.");
        }
    });
}